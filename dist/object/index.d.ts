/**
 * A collection of general functions intended to help with common Object
 * operations.
 * @module object
 */
/**
 * This function is used simply to confirm that a variable is indeed an Object
 * @function
 * @name isObject
 * @param {*} item - The variable whose object status is to be checked.
 * @return {boolean} - True if the variable is an object, false otherwise
 */
export declare const isObject: (item: any) => boolean;
/**
 * This function mimics Object.prototype.assign() but recursively rather than
 * only one level deep.
 * @function
 * @name deepObjectAssign
 * @param {object} target - The object to be assigned into
 * @param {...object} sources - The object from which to copy values
 * @return {object} - The newly assigned Object
 */
export declare const deepObjectAssign: (target: Record<string, any>, ...sources: Record<string, any>[]) => Record<string, any>;
