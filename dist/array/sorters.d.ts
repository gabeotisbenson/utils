/**
 * A collection of Array sorters.  These functions are intended to be passed
 * into Array.prototype.sort.
 * @module array/sorters
 */
declare type SortOrder = 'asc' | 'desc';
declare enum SortResult {
    LT = -1,
    EQ = 0,
    GT = 1
}
/**
 * Used to sort an array of objects by a specified key and in a specified order
 * @function
 * @name byKey
 * @param {string} key - The key by which to sort the object array
 * @param {string} [order=asc] - The order in which to sort the array
 * @returns {function} - A function that compares two objects by the specified
 * key and returns which direction the first item should be moved in the array.
 */
export declare const byKey: (key: string, order?: SortOrder) => (a: Readonly<Record<string, any>>, b: Readonly<Record<string, any>>) => SortResult;
/**
 * Used to sort an array in random order.
 * @function
 * @name shuffle
 * @returns {function} - A function that randomly returns 1 or -1
 */
export declare const shuffle: () => SortResult;
export {};
