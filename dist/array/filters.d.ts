/**
 * A collection of Array filters.  These functions are intended to be passed
 * into Array.prototype.filter.
 * @module array/filters
 */
/**
 * Used to check if an item exists
 * @function
 * @name exists
 * @param {*} item - An item of an array
 * @returns {boolean} - False if item is null or undefined, true otherwise
 */
export declare const exists: (item: unknown) => boolean;
/**
 * Checks if an object is an instance of a class
 * @function
 * @name complexType
 * @param {class} type - The class of which to check if the item is an instance
 * @returns {function} - A function that takes in an item and returns true if
 * the item is an instance of type, false otherwise.
 */
export declare const complexType: (type: any) => (item: unknown) => boolean;
/**
 * Checks if an item has a value for every key in an array
 * @function
 * @name hasValuesForKeys
 * @param {string[]} keys - An array of keys to require the item to have
 * @returns {function} - Returns a function that takes in the item and returns
 * true if it has every key, false otherwise.
 */
export declare const hasValuesForKeys: (keys?: readonly string[]) => (item: Readonly<Record<string, unknown>>) => boolean;
/**
 * Removes duplicate items
 * @function
 * @name removeDuplicates
 * @param {*} item - Current iterated item in the array
 * @param {number} i - Current index of array
 * @param {array} arr - The array being iterated over
 * @returns {boolean} - True if the item is not a duplicate, false otherwise
 */
export declare const removeDuplicates: (item: Readonly<unknown>, i: Readonly<number>, arr: readonly unknown[]) => boolean;
/**
 * Checks if an item is of a simple type
 * @function
 * @name simpleType
 * @param {string} type - The name of a basic type (string, number, boolean,
 * object.
 * @returns {function} - A function that takes in an item and returns true if
 * the item is of the type, false otherwise.
 */
export declare const simpleType: (type: string) => (item: unknown) => boolean;
