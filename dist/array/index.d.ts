/**
 * A collection of general Array functions intended to assist with common array
 * operations.
 * @module array
 */
/**
 * Converts an array to an object with each item in the array being addressible
 * by the specified key.
 * @function
 * @name getKeyIndexedObjectFromArray
 * @param {array} arr - The array of objects
 * @param {string} key - The key by which to index the objects
 * @return {object} - The key-indexed object
 */
export declare const getKeyIndexedObjectFromArray: (arr?: readonly [], key?: string) => Record<string, unknown>;
