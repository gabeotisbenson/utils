/**
 * A collection of functions intended to help with common URL operations.
 * @module url
 */
declare type Param = {
    name: string;
    value: string | number | boolean;
};
/**
 * Appends parameters to a url
 * @function
 * @name appendParams
 * @param {string} url - The url to which parameters should be appended
 * @param {array} params - The parameters to be appended to the url
 * @returns {string} - The new URL
 */
export declare const appendParams: (url?: string, params?: readonly Readonly<Param>[]) => string;
export {};
