import * as filters from './array/filters';
import * as mappers from './array/mappers';
import * as object from './object';
import * as reducers from './array/reducers';
import * as sorters from './array/sorters';
import * as string from './string';
declare const _default: {
    array: {
        filters: typeof filters;
        mappers: typeof mappers;
        reducers: typeof reducers;
        sorters: typeof sorters;
        getKeyIndexedObjectFromArray: (arr?: readonly [], key?: string) => Record<string, unknown>;
    };
    object: typeof object;
    string: typeof string;
};
export default _default;
