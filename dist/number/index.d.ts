/**
 * A collection of functions intended to help with common Number operations.
 * @module number
 */
/**
 * Determines if a number is between two other numbers
 * @function
 * @name isBetween
 * @param {number} min - The lower bound for the between comparison
 * @param {number} max - The upper bound for the between comparison
 * @return {function} - The actual comparison function that takes in the number
 * whose between-ness is to be tested.
 */
export declare const isBetween: (min: number, max: number) => (num: number) => boolean;
/**
 * Returns a random integer between a specified maximum and minimum
 * @function
 * @name randomInt
 * @param {number} min - The lower bound for the integer
 * @param {number} max - The upper bound for the integer
 * @return {number} - A random integer between the two bounds
 */
export declare const randomInt: (min?: number, max?: number) => number;
