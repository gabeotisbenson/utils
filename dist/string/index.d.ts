/**
 * A collection of functions intended to help with common String operations.
 * @module string
 */
/**
 * Converts a string to camel case.
 * E.g. "Gabe's Names & Things" -> "gabesNamesThings"
 * @function
 * @name camelCase
 * @param {string} str - The string to be converted
 * @returns {string} - The formatted string
 */
export declare const camelCase: (str?: string) => string;
/**
 * Converts a string to dash case.
 * E.g. "Gabe's Names & Things" -> "gabes-names-things"
 * @function
 * @name dashCase
 * @param {string} str - The string to be converted
 * @returns {string} - The formatted string
 */
export declare const dashCase: (str?: string) => string;
/**
 * Converts a filepath to camel case.
 * E.g. "src/string/index.js" -> "srcStringIndexJs"
 * @function
 * @name pathToCamel
 * @param {string} str - The filepath string
 * @returns {string} - The formatted string
 */
export declare const pathToCamel: (str?: string) => string;
/**
 * Converts a non-space-delimited string to a space-delimited string
 * E.g. "my-name-is-earl" -> "my name is earl"
 * @function
 * @name spaceCase
 * @param {string} str - The string to reformat
 * @returns {string} - The formatted string
 */
export declare const spaceCase: (str?: string) => string;
/**
 * Converts a table-formatted string to title case.
 * E.g. "ASSOCIATION_USER_REPORT" -> "Association User Report"
 * @function
 * @name tableToTitle
 * @param {string} str - The string to be converted
 * @returns {string} - The formatted string
 */
export declare const tableToTitle: (str?: string) => string;
/**
 * Converts a string to title case.
 * E.g. "this is a title" -> "This Is A Title"
 * @function
 * @name titleCase
 * @param {string} str - The string to be converted
 * @returns {string} - The formatted string
 */
export declare const titleCase: (str?: string) => string;
/**
 * Trims indentation from a multi-line string
 * @function
 * @name trimIndent
 * @param {string} str - The string to be trimmed
 * @returns {string} - The formatted string
 */
export declare const trimIndent: (str?: string) => string;
/**
 * Converts a string to url case.
 * E.g. "Gabe's Names & Things" -> "gabes-names-and-things"
 * @function
 * @name urlCase
 * @param {string} str - The string to be converted
 * @returns {string} - The formatted string
 */
export declare const urlCase: (str?: string) => string;
