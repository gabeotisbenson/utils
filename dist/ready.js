!function(e,n){"object"==typeof exports&&"undefined"!=typeof module?module.exports=n():"function"==typeof define&&define.amd?define(n):(e="undefined"!=typeof globalThis?globalThis:e||self).ready=n()}(this,(function(){"use strict";return function(e){"loading"===document.readyState?document.addEventListener("DOMContentLoaded",e):e()}}));
//# sourceMappingURL=ready.js.map
