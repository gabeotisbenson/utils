declare const ready: (fn: () => void) => void;
export default ready;
