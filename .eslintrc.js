module.exports = {
	extends: ['@gabegabegabe/eslint-config'],
	overrides: [
		{
			files: ['*.ts'],
			extends: '@gabegabegabe/eslint-config/typescript'
		},
		{
			files: 'test/**/*.js',
			rules: {
				'no-magic-numbers': 'off',
				'no-undefined': 'off'
			}
		}
	]
};
