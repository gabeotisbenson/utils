const LOADING_STATE = 'loading';
const LOADED_EVENT = 'DOMContentLoaded';

const ready = (fn: () => void): void => {
	if (document.readyState === LOADING_STATE) document.addEventListener(LOADED_EVENT, fn);
	else fn();
};

export default ready;
