/**
 * A collection of general functions intended to help with common Object
 * operations.
 * @module object
 */

/**
 * This function is used simply to confirm that a variable is indeed an Object
 * @function
 * @name isObject
 * @param {*} item - The variable whose object status is to be checked.
 * @return {boolean} - True if the variable is an object, false otherwise
 */
// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types,@typescript-eslint/no-explicit-any
export const isObject = (item: any): boolean => {
	// eslint-disable-next-line @typescript-eslint/no-unnecessary-condition
	if (item === null) return false;
	// eslint-disable-next-line no-undefined,@typescript-eslint/no-unnecessary-condition
	if (item === undefined) return false;
	if (typeof item !== 'object') return false;
	if (Array.isArray(item)) return false;

	return true;
};

/**
 * This function mimics Object.prototype.assign() but recursively rather than
 * only one level deep.
 * @function
 * @name deepObjectAssign
 * @param {object} target - The object to be assigned into
 * @param {...object} sources - The object from which to copy values
 * @return {object} - The newly assigned Object
 */
// eslint-disable-next-line @typescript-eslint/prefer-readonly-parameter-types,@typescript-eslint/no-explicit-any
export const deepObjectAssign = (target: Record<string, any>, ...sources: Record<string, any>[]): Record<string, any> => {
	if (!sources.length) return target;

	const source = sources.shift();

	if (isObject(target) && isObject(source)) for (const key in source) if (isObject(source[key])) {
		// eslint-disable-next-line no-undefined
		if (target[key] === null || target[key] === undefined) Object.assign(target, { [key]: {} });
		deepObjectAssign(target[key], source[key]);
	} else {
		// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
		Object.assign(target, { [key]: source[key] });
	}

	return deepObjectAssign(target, ...sources);
};
