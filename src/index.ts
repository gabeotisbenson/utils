import * as array from './array';
import * as filters from './array/filters';
import * as mappers from './array/mappers';
import * as object from './object';
import * as reducers from './array/reducers';
import * as sorters from './array/sorters';
import * as string from './string';

export default {
	array: {
		...array,
		filters,
		mappers,
		reducers,
		sorters
	},
	object,
	string
};
