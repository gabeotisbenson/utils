import alias from '@rollup/plugin-alias';
import babel from '@rollup/plugin-babel';
import { eslint } from 'rollup-plugin-eslint';
import fs from 'fs';
import path from 'path';
import resolve from '@rollup/plugin-node-resolve';
import { terser } from 'rollup-plugin-terser';

const OUTPUT_DIR = 'dist';
const ASSET_FILE_TYPES = [];
const SCRIPT_FILE_TYPES = ['.ts'];
const FILE_TYPES = [...ASSET_FILE_TYPES, ...SCRIPT_FILE_TYPES];

// eslint-disable-next-line no-sync
const getRecursiveFileList = filePath => fs
	.readdirSync(path.join(__dirname, filePath), { withFileTypes: true })
	.map(item => {
		const fullPath = `${filePath}/${item.name}`;
		if (item.isDirectory()) return getRecursiveFileList(fullPath);

		return fullPath;
	});

const trimPath = str => str.replace(/^src\//u, '').replace(/\.ts$/u, '');

const getConfig = filePath => ({
	input: { [trimPath(filePath)]: filePath },
	output: {
		dir: OUTPUT_DIR,
		format: 'umd',
		sourcemap: true,
		name: trimPath(filePath)
			.replace(/\//gu, '-')
			.replace(/\./gu, ' ')
			.replace(/[^0-9a-zA-Z\-_ ]/gu, '')
			.replace(/-|_/gu, ' ')
			.replace(/\s+/u, ' ')
			.split(' ')
			.map(word => `${word.charAt(0).toUpperCase()}${word.slice(1).toLowerCase()}`)
			.join('')
			.replace(/^./u, firstLetter => firstLetter.toLowerCase())
	},
	plugins: [
		alias({
			'~': path.join(__dirname, 'src'),
			resolve: FILE_TYPES
		}),
		resolve({ browser: true, extensions: FILE_TYPES }),
		eslint({ include: ['**/*.ts'] }),
		babel({
			babelHelpers: 'bundled',
			extensions: SCRIPT_FILE_TYPES,
			exclude: 'node_modules/**'
		}),
		terser()
	]
});

export default getRecursiveFileList('src')
	.flat(Infinity)
	.map(filePath => getConfig(filePath));
