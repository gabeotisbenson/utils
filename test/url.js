import { appendParams } from '~/url';
import test from 'ava';

test('appendParams', t => {
	const url = 'https://gabe.xyz';
	const params = [
		{ name: 'age', value: 31 },
		{ name: 'hair', value: 'brown' },
		{ garbage: 'yeah', wontwork: 'also yeah' },
		{ name: 'novalue', value: null },
		{ name: 'alsonovalue' }
	];
	const expectedOutput = `${url}?age=31&hair=brown&novalue&alsonovalue`;

	t.is(appendParams(url, params), expectedOutput);
});
