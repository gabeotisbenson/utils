import test from 'ava';
import {
	deepObjectAssign,
	isObject
} from '~/object';

// 'isObject' Tests
test('Is a string an object', t => t.is(isObject('gabe'), false));
test('Is a number an object', t => t.is(isObject(5), false));
test('Is an array an object', t => t.is(isObject([]), false));
test('Is an object an object', t => t.is(isObject({}), true));

// 'deepObjectAssign' Tests
test('deepObjectAssign', t => {
	const input = {
		source: { name: 'gabe', age: 30, friend: { name: 'tyler' } },
		target: { home: 'athens', friend: { home: 'dalton' } }
	};

	const output = {
		name: 'gabe',
		age: 30,
		home: 'athens',
		friend: {
			name: 'tyler',
			home: 'dalton'
		}
	};

	t.deepEqual(deepObjectAssign(input.target, input.source), output);
});
