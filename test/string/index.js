import { ioTest } from '../_io-test';
import test from 'ava';
import {
	camelCase,
	dashCase,
	pathToCamel,
	spaceCase,
	tableToTitle,
	titleCase,
	trimIndent,
	urlCase
} from '~/string';

// Test Camelcase
test('camelcase', t => {
	const inputs = [
		'dog',
		'CAT',
		'cat-dog',
		'cat dog',
		'CAT dog',
		'CAT & DOG 3, Dancing with it\'s stars!',
		'cat_dog',
		null,
		undefined
	];
	const outputs = [
		'dog',
		'cat',
		'catDog',
		'catDog',
		'catDog',
		'catDog3DancingWithItsStars',
		'catDog',
		'',
		''
	];
	ioTest(inputs, outputs, camelCase, t);
});

// Test dashCase
test('dashCase', t => {
	const input = 'Gabe\'s Names & Things';
	const expected = 'gabes-names-things';

	t.is(dashCase(input), expected);
});

// Test pathToCamel
test('pathToCamel', t => {
	const input = 'src/string/index.js';
	const expected = 'srcStringIndexJs';

	t.is(pathToCamel(input), expected);
});

// Test spaceCase
test('spaceCase', t => {
	const inputs = [
		'my-name-is-earl',
		'my_name_is_earl',
		'my,name,is,earl',
		'my  name  is  earl'
	];
	const expected = 'my name is earl';

	ioTest(inputs, [...new Array(inputs.length)].map(() => expected), spaceCase, t);
});

// Test TitleCase
test('titleCase', t => {
	const inputs = [
		'hello',
		'hello_my_name_is_gabe',
		'the_great_zig_ziglar\'s_dragon_&_other_tales',
		null,
		undefined,
		5
	];
	const outputs = [
		'Hello',
		'Hello My Name Is Gabe',
		'The Great Zig Ziglar\'s Dragon & Other Tales',
		'',
		'',
		'5'
	];
	ioTest(inputs, outputs, titleCase, t);
});

// Test TableToTitle
test('tableToTitle', t => {
	const inputs = [
		'TABLE',
		'TWO_TABLE',
		'THREE_WHOLE_TABLE',
		null,
		undefined
	];
	const outputs = [
		'Table',
		'Two Table',
		'Three Whole Table',
		'',
		''
	];
	ioTest(inputs, outputs, tableToTitle, t);
});

// 'trimIndent' Tests
test('trimIndent', t => {
	const input = `
		This is an example of a multi-line string.  It has to be fairly long to
		justify having multiple lines.  You see when you have a string like this you
		may just be wrapping it to make it more readable in code, but you're going
		to end up with multiple lines and each are going to be indented based on how
		many tabs are over there <---.  Problematic!  Luckily, this function should
		fix that for you.
	`;
	const output = 'This is an example of a multi-line string.  It has to be fairly long to\njustify having multiple lines.  You see when you have a string like this you\nmay just be wrapping it to make it more readable in code, but you\'re going\nto end up with multiple lines and each are going to be indented based on how\nmany tabs are over there <---.  Problematic!  Luckily, this function should\nfix that for you.';

	t.is(trimIndent(input), output);
});

// Test urlCase
test('urlCase', t => {
	const input = 'Gabe\'s Names & Things';
	const expected = 'gabes-names-and-things';

	t.is(urlCase(input), expected);
});
