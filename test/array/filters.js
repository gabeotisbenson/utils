import test from 'ava';
import {
	complexType,
	exists,
	hasValuesForKeys,
	removeDuplicates,
	simpleType
} from '~/array/filters';

test('exists', t => {
	// eslint-disable-next-line no-undefined
	const undefinedVal = undefined;
	const input = [undefinedVal, null, 'gabe'];
	const output = ['gabe'];

	t.deepEqual(input.filter(exists), output);
});

test('complexType', t => {
	const input = {
		test1: [new Date('1988-11-28'), '1988-11-28', new Date('2000-01-01')],
		test2: [new URL('https://gabe.xyz'), new URL('https://duckduckgo.com'), 'https://google.com']
	};
	const output = {
		test1: [new Date('1988-11-28'), new Date('2000-01-01')],
		test2: [new URL('https://gabe.xyz'), new URL('https://duckduckgo.com')]
	};

	t.deepEqual(input.test1.filter(complexType(Date)), output.test1);
	t.deepEqual(input.test2.filter(complexType(URL)), output.test2);
});

test('hasValuesForKeys', t => {
	const input = [
		{
			name: 'gabe',
			age: 30,
			hometown: 'dalton'
		},
		{
			name: 'rae',
			age: 25
		}
	];
	const output = [input[0]];
	const requiredKeys = ['name', 'age', 'hometown'];

	t.deepEqual(input.filter(hasValuesForKeys(requiredKeys)), output);
});

test('removeDuplicates', t => {
	const input = ['gabe', 'rae', 'gabe'];
	const expected = ['gabe', 'rae'];

	t.deepEqual(input.filter(removeDuplicates), expected);
});

test('simpleType', t => {
	const num = 5;
	const input = ['gabe', num, 'rae'];
	const output = [num];

	t.deepEqual(input.filter(simpleType('number')), output);
});
