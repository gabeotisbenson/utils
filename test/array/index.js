import { getKeyIndexedObjectFromArray } from '~/array';
import test from 'ava';

test('getKeyIndexedObjectFromArray', t => {
	// Test some using 'name' as a key
	const test1Key = 'name';
	const test1Input = [
		{
			name: 'gabe',
			age: 30
		},
		{
			name: 'rae',
			age: 25
		},
		{
			name: 'tilly',
			age: 28
		}
	];
	const test1Output = {
		gabe: { name: 'gabe', age: 30 },
		rae: { name: 'rae', age: 25 },
		tilly: { name: 'tilly', age: 28 }
	};
	t.deepEqual(getKeyIndexedObjectFromArray(test1Input, test1Key), test1Output);

	// Test an empty array
	const test2Key = 'bogus';
	const test2Input = [];
	const test2Output = {};
	t.deepEqual(getKeyIndexedObjectFromArray(test2Input, test2Key), test2Output);

	// Test single-item array using 'email' as a key
	const test3Key = 'email';
	const test3Input = [{ email: 'me@gabe.xyz', username: 'gabeotisbenson' }];
	const test3Output = { 'me@gabe.xyz': { email: 'me@gabe.xyz', username: 'gabeotisbenson' } };
	t.deepEqual(getKeyIndexedObjectFromArray(test3Input, test3Key), test3Output);
});
