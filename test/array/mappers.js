import test from 'ava';
import { toKey } from '~/array/mappers';

test('toKey', t => {
	const input = [{ name: 'gabe' }, { name: 'rae' }, { name: 'tilly' }];
	const output = ['gabe', 'rae', 'tilly'];

	t.deepEqual(input.map(toKey('name')), output);
});
