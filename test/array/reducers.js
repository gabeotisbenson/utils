import test from 'ava';
import {
	allTrue,
	atLeastOneTrue,
	avg,
	max,
	min,
	singleItem,
	singleItemOrNull,
	sum
} from '~/array/reducers';

const testNums = [
	1,
	3,
	5,
	7,
	9
];

test('allTrue', t => {
	const input1 = [true, true, true];
	const input2 = [true, false, true];
	const expected1 = true;
	const expected2 = false;

	t.is(input1.reduce(allTrue), expected1);
	t.is(input2.reduce(allTrue), expected2);
});

test('atLeastOneTrue', t => {
	const input1 = [false, true, false];
	const input2 = [false, false, false];
	const expected1 = true;
	const expected2 = false;

	t.is(input1.reduce(atLeastOneTrue), expected1);
	t.is(input2.reduce(atLeastOneTrue), expected2);
});

test('avg', t => {
	const expected = 5;

	t.is(testNums.reduce(avg), expected);
});

test('max', t => {
	const expected = 9;

	t.is(testNums.reduce(max), expected);
});

test('min', t => {
	const expected = 1;

	t.is(testNums.reduce(min), expected);
});

test('singleItem', t => {
	const input = ['gabe'];
	const expected = 'gabe';

	t.is(input.reduce(singleItem), expected);
});

test('singleItemOrNull', t => {
	const input1 = ['gabe'];
	const input2 = [];
	const expected1 = 'gabe';
	const expected2 = null;

	t.is(input1.reduce(...singleItemOrNull), expected1);
	t.is(input2.reduce(...singleItemOrNull), expected2);
});

test('sum', t => {
	const expected = 25;

	t.is(testNums.reduce(sum), expected);
});
