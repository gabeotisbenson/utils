import test from 'ava';
import { byKey, shuffle } from '~/array/sorters';

test('byKey', t => {
	const input = [
		{ name: 'tilly', age: 29 },
		{ name: 'gabe', age: 30 },
		{ name: 'rae', age: 25 }
	];

	const expectedByName = [
		{ name: 'gabe', age: 30 },
		{ name: 'rae', age: 25 },
		{ name: 'tilly', age: 29 }
	];

	const expectedByAge = [
		{ name: 'rae', age: 25 },
		{ name: 'tilly', age: 29 },
		{ name: 'gabe', age: 30 }
	];

	t.deepEqual(input.sort(byKey('name')), expectedByName);
	t.deepEqual(input.sort(byKey('age')), expectedByAge);
});

test('shuffle', t => {
	// eslint-disable-next-line no-magic-numbers
	const input = [1, 2, 3, 4, 5];

	t.notDeepEqual([...input].sort(shuffle), input);
});
