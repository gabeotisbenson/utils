import test from 'ava';
import { isBetween, randomInt } from '~/number';

// 'isBetween' Test
test('isBetween', t => {
	t.is(isBetween(1, 3)(2), true);
	t.is(isBetween(1, 3)(4), false);
	t.is(isBetween(1, 3)(1), true);
	t.is(isBetween(1, 3)(3), true);
});

// Random integer test
test('randomInt', t => {
	t.is(typeof randomInt(), 'number');
	t.true(randomInt(5) >= 5);
	const r = randomInt(5, 10);
	t.true(r >= 5 && r <= 10);
});
