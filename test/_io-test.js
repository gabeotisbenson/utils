export const ioTest = (inputs = [], outputs = [], fn = a => a, t) => {
	inputs.forEach((input, i) => {
		t.is(fn(input), outputs[i]);
	});
};
